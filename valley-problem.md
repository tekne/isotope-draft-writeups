# The Valley Problem and Extensional Nuclei

One issue when trying to implement an extensional theorem prover is what I have come to call the "valley problem." 
It comes from the fact that while a rule of the form [^notation]
```math
\frac{\Gamma \vdash p: x =_A y}{\Gamma \vdash x \equiv y: A}
```
can be consistent, giving us [a (definitionally) extensional type theory](https://ncatlab.org/nlab/show/extensional+type+theory#definitional_extensionality), recognizing judgemental equality now depends on context.

[^notation]: Here, $`\equiv`$ denotes judgemental equality and $`=_A`$ denotes propositional equality of terms of type $`A`$, as in the [HoTT book](https://homotopytypetheory.org/book/)
