# Isotope ISA draft

## Overview

`isotope` is a loose hybrid of an [RVSDG](https://arxiv.org/abs/1912.05036) and a [CPS](https://en.wikipedia.org/wiki/Continuation-passing_style) program. `isotope` programs are graphs composed of instruction-nodes which have multiple input and output values. In particular, we have
- *Primitive instructions*, which represent a primitive operation
- *Compound instructions*, which contain a subgraph, or *region*, within them; this subgraph may often depend on external values, which are simply treated as inputs to the enclosing nodes
Input and output values are subdivided into three sorts:
- *Data values*, which represent pieces of plain data. In particular, unlike, e.g., in C, there is no notion of pointer provenance or such for a *value*; these are part of the semantics for
- *States*, which represent external state as abstract mathematical objects. There are also
- *Labels*, which represent a generalized notion of control flow

## Values

### Data
*Data values* can be thought of as trees with *primitive* data type leaves, where the primitive data types are:
- $`n`$-bit integers, where $`0 \leq n < 2^{32}`$. 
    * Integers are defined bitwise, with each bit being either 0, 1, unspecified but fixed, or poison (capable of being anything including inconsistent choices).
    * The language defines primitive integer constants of all bit-widths
- [IEEE 754 floating point numbers](https://en.wikipedia.org/wiki/IEEE_754)
    * The language defines primitive floating-point constants of all supported bit-widths, along with `NAN`, `INF`, etc. These are supported on all platforms, and emulated in software where hardware instructions are unavailable.
- Pointer-size integers `iptr` and machine-size integers `isize`. Note we do *not* necessarily have `iptr == isize`, e.g. on CHERI, but most platforms, e.g. x86, may assume this. Consider also "capability-size integers `icap`", to internalize CHERI yet further.
- Function pointers and functors, parametrized by argument types, return types, and calling convention. Functors may be defined in "sets", so that any functor in the set can be passed to the generated functor type.

We then introduce the following type formers:
- *Arrays* of a type length $`0 \leq n < 2^{32}`$. 
    * Strings are just arrays of bytes
    * Vector operations (for SIMD support) are represented simply as builtin operations on arrays. 
- *Structs*, which consist of a tuple of types and a *layout*, determining alignment, padding, and whether the compiler is allowed to re-order fields to save memory. We may also declare opaque structs. A struct without a layout, field names, or a name is called a *tuple*.
- *Unions*, which consist of a set of types and are defined to have the weakest possible layout which can contain all types in the set.

### States
A state represents the set of memory addresses, including virtual addresses representing e.g. the operating system (we pretend "everything is an MMIO device," even if the user never actually sees the corresponding pointer) which may be read or modified. Conceptually, every byte may or may not be:
- *Readable*. If *readable*, the byte may or may not be *thread-immutable* or *cell* (no operations outside the current thread of execution may modify the byte between reads), and may or may not be *immutable* (no operations not explicitly passed the state of the byte may affect it's value).
- *Writable*. If writable, it may be required to write to the byte using atomics to avoid data races.

### Labels
A label represents an action in control flow. It can be
- A jump to a basic block
- A `return` instruction or tail-call, passed a collection of values and states.
- An `abort` instruction, which may compile into a platform-specific halt-instruction, infinite loop, panic, etc. Think Haskell's `Nontermination` exception.
- An `undefined` instruction. It is UB for control flow to ever reach such an instruction; this may be placed in a switch statement, and is the default value for any unspecified branches.
- A switch, choosing between labels based on a condition
- Shims, which are another label with states potentially reshuffled, combined, etc. (but no value operations performed)
Labels are typed, returning a collection of, potentially named, values and states.

## Instructions

### Arithmetic and Logic
We provide the standard complement of arithmetic and logic instructions, including
- Bitwise operations (AND/OR/etc.)
- Signed and unsigned integer arithmetic
- IEEE 754 floating point arithmetic, including special functions (e.g. trigonometry)
- Array variants of all such operations
- Sign/zero extension, truncation, etc
- Interpretation of floating points, etc, as integers

### Memory Access
In terms of memory access, we provide
- A `load` instruction, taking in a pointer, an ordering (for atomicity, can also be marked as unsynchronized), and a state, and returning the post-load state (for sequencing purposes, as otherwise there's no way to guarantee a load happens *before* some operation) and loaded value.
- A `store` instruction, again taking in a pointer, an ordering (for atomicity, can also be marked as unsynchronized), and a state, and returning the post-store state
- A fallible `cas` instruction, taking in a pointer, an ordering, and a state, and returning the post-store state and either an error or the old value. We may also include other atomic primitives

We also provide the following basic operations on states:
- A `split` operation which splits one state into `n` concurrent states (whether they are parallel or not depends on the `join` used); this is implicitly inserted whenever multiple instructions depend on one state (as states are pseudo-linear in this way; in particular, they are equipped with a comonoid structure), but can be inserted explicitly as well.
- A `join` operation, as well as a `parallel join` operation, which is a partial monoid on states. In a `join` operation, the two joined "threads" see instructions ordered strictly according to their state dependencies, whereas in a `parallel join` operation they see instructions ordered according to their (weaker) atomic dependencies; for example, two relaxed stores will be viewed in order by two "threads" in a `join` but not by two "threads" in a `parallel join`.
- A `sequential join` operation, which has the semantics that, where `s` is a state, both `(A;B)(s) := B(A(s))` and `(B;A)(s) := A(B(s))` refine `sequential_join((a, b)(split(s)))`

### Memory Assertions
In addition, we provide the following *assertions* on states:
- `lock`: given a memory range and a state, the memory range may not be modified through any other state until a corresponding `unlock` instruction
- `freeze`: given a memory range and a state, the memory range may not be modified through *any* state, including this one, until a corresponding `thaw` instruction
We also consider adding in assertions to render memory, e.g., unreadable, and to ban explicitly *concurrent* access/modifications, later. See Rust's [`Cell`](https://doc.rust-lang.org/std/cell/struct.Cell.html) type for some inspiration.
